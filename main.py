from fastapi import FastAPI
from pydantic import BaseModel
from fastapi.middleware.cors import CORSMiddleware
import joblib

app = FastAPI()
app.add_middleware(
    CORSMiddleware,
    allow_origins=["http://localhost:5173"],
    allow_credentials=True,
    allow_methods=["GET", "POST", "PUT", "DELETE"],
    allow_headers=["*"],
)

# Load the trained model
usemodel = joblib.load('PricePredict.pkl')

class CarInput(BaseModel):
    Brand: int
    Year: int
    Model: int
    Engine: int
    Kilometres: float

class CarPrice(BaseModel):
    Price: float

@app.post("/car/predict/")
async def predict_car_price(car: CarInput):
    try:
        data = [[car.Brand, car.Year, car.Model, car.Engine, car.Kilometres]]
        prediction = usemodel.predict(data)
        
        return {"Price": prediction[0]}  # Use square brackets and "Price" key
    except Exception as e:
        print("Error prediction:", e)
        return {"error": "Prediction failed"}

